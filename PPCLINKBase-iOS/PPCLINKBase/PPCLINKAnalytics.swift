//
//  PPCLINKAnalytics.swift
//  PPCLINKBase-iOS
//
//  Created by Pham Diep on 5/14/20.
//  Copyright © 2020 PPCLink. All rights reserved.
//

import Foundation
import FirebaseAnalytics

@objc class PPCLINKAnalytics: NSObject {
    @objc static var shared = PPCLINKAnalytics.init()
    @objc func trackLogEvents(itemID: String, contentType: String) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: itemID,
            AnalyticsParameterContentType: contentType
        ])
        
    }
}
